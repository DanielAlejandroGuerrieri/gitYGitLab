function stringModifier(inputString) {
/* ***
 * 
 * Return expected: tSrOiDcBaA
 * 
*** */
  let array = inputString.split('').sort().reverse();
  
  let upperCase = true;
  let result = array.map(el => {
    if(upperCase) {
      upperCase = !upperCase;
      return el.toLowerCase();
    } else {
      upperCase = !upperCase;
      return el.toUpperCase();
    }
  })

  return result.join();
}


module.exports = {
  stringModifier,
};
