function numberModifier(inputNumber) {
/* ***
 *
 * You can only use divisions
 * 
 * Return expected: 159
 * 
 * 
*** */ 
return inputNumber/16;

}

module.exports = {
  numberModifier,
};
